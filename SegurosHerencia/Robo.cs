﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SegurosHerencia
{
    class Robo : Seguro
    {
        public string Modelo { get; set; }
        public int Tamaño { get; set; }
        public int PrecioActual { get; set; }
        public DateTime FechaCompra { get; set; }
        public override bool SiniestroValido(int Dni)
        {
            if (Tomador.Dni == Dni && Tamaño < 6)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

    }
}
