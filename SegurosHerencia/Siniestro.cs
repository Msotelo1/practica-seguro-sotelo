﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SegurosHerencia
{
    class Siniestro
    {
        public DateTime Fecha { get; set; }
        public int NumeroPoliza { get; set; }
        public Persona ClienteInformador { get; set; }
        public Siniestro(DateTime Fecha, int NroPoliza, Persona ClienteInformador)
        {
            this.Fecha = Fecha;
            this.NumeroPoliza = NroPoliza;
            this.ClienteInformador = ClienteInformador;
        }
    }
}
