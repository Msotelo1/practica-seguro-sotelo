﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace SegurosHerencia
{
    class Automotores : Seguro
    {
        public string Patente { get; set; }
        public string Marca { get; set; }
        public DateTime AñoFab { get; set; }
        public tipoAuto Tipo { get; set; }
        public int CantMaxOcup { get; set; }
        public override bool SiniestroValido(int Dni)
        {
            if (Beneficiario.Dni == Dni && Regex.IsMatch(Patente, "/w{2}/d{3}/w{2}"))
            {
                if (Tipo == tipoAuto.Moto && AñoFab.Year.CompareTo(DateTime.Today) < 5)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
    }
}
