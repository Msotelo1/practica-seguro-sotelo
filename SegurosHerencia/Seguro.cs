﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SegurosHerencia
{
    abstract class Seguro
    {
        public Persona Tomador { get; set; }
        public Persona Beneficiario { get; set; }
        public int Prima { get; set; }
        public int NumeroPoliza { get; set; }
        public int Mensual { get; set; }
        public int CalcularPorcentajeCobertura()
        {
            int PorcentajeCobertura = (Mensual / Prima) * 100;

            return PorcentajeCobertura;
        }
        public abstract bool SiniestroValido(int Dni);


    }
}
