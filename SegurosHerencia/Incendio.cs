﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SegurosHerencia
{
    class Incendio : Seguro
    {
        public int m2 { get; set; }
        public int CantMatafuegos { get; set; }
        public int CantEnchufes { get; set; }
        public bool Vivienda { get; set; }
        public int MontoBines { get; set; }
        public override bool SiniestroValido(int Dni)
        {
            if (Dni == Tomador.Dni && CantMatafuegos >= CantEnchufes)
            {
                if (Vivienda == false)
                {
                    return true;
                }
                else
                {
                    if (m2 < 400)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            else
            {
                return false;
            }  
        }



    }
}
