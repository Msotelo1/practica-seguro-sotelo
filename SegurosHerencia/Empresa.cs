﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace SegurosHerencia
{
    public class Empresa
    {
        List<Seguro> Seguros = new List<Seguro>();
        List<Siniestro> Siniestros = new List<Siniestro>();

        public int ObtenerPorcentajeCobertura(int NumeroPoliza)
        {
            int PorcentajeCobertura = 0;

            foreach (Seguro seguro in Seguros)
            {
                if (NumeroPoliza == seguro.NumeroPoliza)
                {
                    PorcentajeCobertura = seguro.CalcularPorcentajeCobertura();
                }
            }
            return PorcentajeCobertura;
        }

        
        public bool RegistararSiniestro(int Dni, DateTime Fecha,int NumeroPoliza,Persona ClienteInformador)
        {
            foreach (Seguro seguro in Seguros)
            {
                if (seguro.NumeroPoliza == NumeroPoliza)
                {
                    if (seguro.SiniestroValido(Dni) == true)
                    {
                        Siniestro siniestro = new Siniestro(Fecha, NumeroPoliza, ClienteInformador);
                        Siniestros.Add(siniestro);
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }

            return false;
        }
    }
}
