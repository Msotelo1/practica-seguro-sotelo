﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SegurosHerencia
{
    class SiniestrosLista
    {
        public string NombreApellido { get; set; }
        public DateTime Fecha { get; set; }
        public tipoSeguro Tipo { get; set; }
    }
}
